const fs = require('fs')
const setup = require('axios-cache-adapter').setup
const csvtojson = require('csvtojson')
const normalizations = require('./normalizations')

const casesWorldURL =
  'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'

const deathsWorldURL =
  'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'

const casesUSURL =
  'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv'

const deathsUSURL =
  'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv'

const cachedAxios = setup({
  cache: {
    maxAge: 15 * 60 * 1000
  }
})
const json = {
  regions: []
}
let casesWorldJson, casesUSJson, deathsWorldJson, deathsUSJson, populationsWorldJson, populationsUSJson, statesGovernorJson

async function init () {
  try {
    const casesWorldCsv = await getRemoteCSV(casesWorldURL, 'cases-world')
    const casesUSCsv = await getRemoteCSV(casesUSURL, 'cases-us')
    const deathsWorldCsv = await getRemoteCSV(deathsWorldURL, 'deaths-world')
    const deathsUSCsv = await getRemoteCSV(deathsUSURL, 'deaths-us')

    const opt = {
      trim: true,
      checkColumn: true
    }

    casesWorldJson = await csvtojson(opt).fromString(casesWorldCsv)
    casesUSJson = await csvtojson(opt).fromString(casesUSCsv)
    deathsWorldJson = await csvtojson(opt).fromString(deathsWorldCsv)
    deathsUSJson = await csvtojson(opt).fromString(deathsUSCsv)
    populationsWorldJson = await csvtojson(opt).fromFile(
      'populationsWorld.csv'
    )
    populationsUSJson = await csvtojson(opt).fromFile(
      'populationsUS.csv'
    )
    statesGovernorJson = await csvtojson(opt).fromFile(
      'statesGovernor.csv'
    )

    json.dates = Object.keys(casesWorldJson[0]).slice(4, casesWorldJson[0].length)

    createNationRows(
      casesWorldJson,
      populationsWorldJson
    )

    addGlobalProvence(casesWorldJson)

    createUSStatesRows(
      casesUSJson,
      populationsUSJson
    )
    // addStatesUSStates(casesUSJson, deathsUSJson, populationsUSJson, [])

    addUSCounties(casesUSJson, populationsUSJson, [])
    console.log(
      'Regions included',
      json.regions.length
    )
    json.regions = json.regions
      .filter((el) => el.cases > 0 || el.deaths > 0)
      .sort((a, b) => b.cases - a.cases)

    console.log(
      'After removing regions with no cases or deaths',
      json.regions.length
    )

    fs.writeFileSync('data.json', JSON.stringify(json))
    fs.writeFileSync('dataPretty.json', JSON.stringify(json, null, 2))

    const hennepin = json.regions.find(el => el.countyName === 'Hennepin')
    fs.writeFileSync('hennepin.json', JSON.stringify(hennepin, null, 2))
  } catch (error) {
    console.log('ERROR', error)
  }
}

init()

function addUSCounties (casesUSJson, populationsUSJson) {
  for (const row of casesUSJson) {
    if (row.Admin2 === 'Unassigned') {
      // Is the row we used to create the stateName
      continue
    }

    // row.FIPS = parseInt(row.FIPS)
    const countyFIPS = parseInt(
      row.FIPS.substring(row.FIPS.length - 5, row.FIPS.length - 2)
    )
    const pop = populationsUSJson.find(function (el) {
      return (
        Number(el.COUNTY) === countyFIPS && el.STNAME === row.Province_State
      )
    })

    if (!pop) {
      console.log(
        'countyName with unknown population, not adding entry',
        row.Admin2,
        row.Province_State,
        countyFIPS,
        row.Province_State
      )
      continue
    }

    const casesHistory = Object.values(row).slice(11, row.length).map(Number)
    const deathsHistory = getDeathsHistoryUS(row.Province_State, row.Admin2)
    if (casesHistory[casesHistory.legth - 1] === 0) {
      console.log(
        'countyName with no cases, probably moved to another countyName in the data',
        row.Admin2,
        row.Province_State
      )
      continue
    }

    if (row.Admin2 === 'Hennepin') {
      console.log('countyName cases length:', casesHistory.length)
      console.log('countyName deaths length:', deathsHistory.length)
    }

    updateParentRegionsTotals(normalizeName(row.Country_Region), row.Province_State, casesHistory, deathsHistory)

    const newRow = {
      cases: Number(casesHistory[casesHistory.length - 1]),
      casesHistory,
      countyName: row.Admin2,
      deaths: Number(deathsHistory[deathsHistory.length - 1]),
      deathsHistory,
      nationName: 'United States',
      population: Number(pop.population),
      regionName: row.Admin2,
      regionType: 'countyName',
      stateName: row.Province_State
    }

    json.regions.push(newRow)
  }

  console.log('After adding counties', json.regions.length)
}

function updateParentRegionsTotals (nationName, stateName, casesHistory, deathsHistory) {
  // console.log(nationName, stateName)
  const regionRowIndex = json.regions.findIndex(function (el) {
    return el.nationName === nationName && el.stateName === stateName && !el.countyName
  })
  // console.log('regionRowIndex', regionRowIndex)

  incrementRegionTotals(regionRowIndex, casesHistory, deathsHistory)

  const nationRowIndex = json.regions.findIndex(function (el) {
    return el.nationName === nationName && !el.stateName
  })
  // console.log('nationRowIndex', nationRowIndex)
  incrementRegionTotals(nationRowIndex, casesHistory, deathsHistory)
}

function incrementRegionTotals (regionIndex, casesHistory, deathsHistory) {
  json.regions[regionIndex].cases += Number(casesHistory[casesHistory.length - 1])
  json.regions[regionIndex].casesHistory.forEach(
    function (o, i, a) {
      a[i] += Number(casesHistory[i])
    }
  )
  json.regions[regionIndex].deaths += Number(deathsHistory[deathsHistory.length - 1])
  json.regions[regionIndex].deathsHistory.forEach(
    function (o, i, a) {
      a[i] += Number(deathsHistory[i])
    }
  )
}

function createUSStatesRows (casesUSJson, deathsWorldJson, populationsWorldJson) {
  for (const row of casesUSJson) {
    if (row.Admin2 !== 'Unassigned') {
      // We're using rows with this value to create the stateName
      continue
    }
    const casesHistory = Object.values(row).slice(11, row.length).map(Number)
    const deathsHistory = getDeathsHistoryUS(row.Province_State, row.Admin2)

    const pop = populationsUSJson.find(
      (el) => el.STNAME === row.Province_State && el.COUNTY === '0'
    )
    if (!pop) {
      console.log('ERROR! Skipping expected population for stateName', row.Province_State)
      continue
    }
    const governorParty = statesGovernorJson.find(function (el) {
      return (
        el.State === row.Province_State
      )
    })

    if (!governorParty) {
      console.log('no governor for', row.Province_State)
    }

    const newRow = {
      cases: Number(casesHistory[casesHistory.length - 1]),
      casesHistory,
      deaths: Number(deathsHistory[deathsHistory.length - 1]),
      deathsHistory,
      hasSubregions: true,
      nationName: 'United States',
      party: governorParty,
      population: Number(pop.population),
      regionName: row.Province_State,
      stateName: row.Province_State
    }

    json.regions.push(newRow)
  }
}

function getDeathsHistoryUS (stateName, countyName) {
  // console.log(stateName, countyName)
  const deathsRow = deathsUSJson.find(
    (el) => el.Province_State === stateName && ((!countyName && el.Admin2 === 'Unassigned') || (countyName && el.Admin2 === countyName))
  )
  const values = Object.values(deathsRow)
  // console.log(values.slice(11, values.length)[0])
  return values.slice(12, values.length).map(Number)
}

function addGlobalProvence (casesWorldJson) {
  /*
  No provence populations yet, just add data to nationName rows
  */
  for (const row of casesWorldJson) {
    if (nationTotalsAreDefinedInSubregions(row['Country/Region'])) {
      const nationRowIndex = json.regions.findIndex(el => el.regionName === row['Country/Region'] && !el.stateName)
      if (nationRowIndex === -1) {
        console.log('ERROR! Cannot find expected row for nationName with subregions', row['Country/Region'])
        continue
      }
      const casesValues = Object.values(row).slice(4, row.length).map(Number)
      json.regions[nationRowIndex].cases += Number(casesValues[casesValues.length - 1])

      json.regions[nationRowIndex].casesHistory.forEach(function (o, i, a) {
        // console.log(a.length, deathsValues.length)
        a[i] += Number(casesValues[i])
      })

      const deathsValues = getGlobalDeathsHistory(row['Country/Region'], row['Province/stateName'])
      json.regions[nationRowIndex].deaths += Number(deathsValues[deathsValues.length - 1])
      json.regions[nationRowIndex].deathsHistory.forEach(function (o, i, a) {
        // console.log(a.length, deathsValues.length)
        a[i] += Number(deathsValues[i])
      })
    }
  }
}

function createNationRows (casesWorldJson, populationsWorldJson) {
  let hasSubregions,
    casesHistory,
    deathsHistory,
    nationName,
    population,
    regionName,
    cases,
    deaths

  for (const row of casesWorldJson) {
    const values = Object.values(row).map(Number)

    if (nationTotalsAreDefinedInSubregions(row['Country/Region']) && regionHasBeenAdded(normalizeName(row['Country/Region']))) {
      // console.log('Totals defined in states, row already added:', row['Country/Region'])
      continue
    } else if (nationTotalsAreDefinedInSubregions(row['Country/Region'])) {
      // console.log('Totals defined in states, adding row:', row['Country/Region'])
      const normalizedName = normalizeName(row['Country/Region'])
      const pop = populationsWorldJson.find(
        (el) => el.name === normalizedName
      )
      if (!pop) {
        console.log('ERROR! Skipping expected population for nationName ', normalizedName)
        continue
      }
      hasSubregions = true
      casesHistory = new Array(values.length - 4).fill(0)
      deathsHistory = new Array(values.length - 4).fill(0)
      nationName = normalizedName
      population = pop.population * 1000
      regionName = normalizedName
      cases = 0
      deaths = 0
    } else if (row['Province/stateName']) {
      // console.log('Adding territory:', row['Province/stateName'])
      const normalizedName = normalizeName(row['Province/stateName'])
      const pop = populationsWorldJson.find(
        (el) => el.name === normalizedName
      )
      if (!pop) {
        console.log('ERROR! Skipping expected population for provence ', normalizedName)
        continue
      }

      hasSubregions = false
      casesHistory = values.slice(11, row.length).map(Number)
      deathsHistory = getGlobalDeathsHistory(row['Country/Region'], row['Province/stateName'])
      nationName = row['Province/stateName']
      population = pop.population * 1000
      regionName = row['Province/stateName']
      cases = Number(values[values.length - 1])
      deaths = getGlobalDeaths(row['Country/Region'], row['Province/stateName'])
    } else {
      // console.log('Is nationName, adding row:', row['Country/Region'])
      const normalizedName = normalizeName(row['Country/Region'])
      const pop = populationsWorldJson.find(
        (el) => el.name === normalizeName(row['Country/Region'])
      )
      if (!pop) {
        console.log('ERROR! Skipping expected population for nationName ', normalizedName)
        continue
      }
      hasSubregions = false
      casesHistory = values.slice(11, row.length).map(Number)
      deathsHistory = getGlobalDeathsHistory(row['Country/Region'])
      nationName = row['Country/Region']
      population = pop.population * 1000
      regionName = row['Country/Region']
      cases = Number(values[values.length - 1])
      deaths = getGlobalDeaths(row['Country/Region'])
    }

    json.regions.push({
      cases,
      casesHistory,
      deaths,
      deathsHistory,
      hasSubregions,
      nationName,
      population,
      regionName,
      regionType: 'nation'
    })
  }
}

function getGlobalDeaths (nationName, provenceName) {
  const values = getGlobalDeathsHistory(nationName, provenceName)
  return values[values.length - 1]
}

function getGlobalDeathsHistory (nationName, provenceName) {
  const deathsRow = deathsWorldJson.find(
    (el) => el['Country/Region'] === nationName && ((!provenceName && !el['Province/stateName']) || (provenceName && el['Province/stateName'] === provenceName))
  )
  const values = Object.values(deathsRow)
  return values.slice(4, values.length).map(Number)
}

function regionHasBeenAdded (nationName, stateName) {
  return json.regions.some(
    (el) => el.nationName === nationName && (!stateName || el.stateName === stateName)
  )
}

function nationTotalsAreDefinedInSubregions (regionName) {
  const passed = ['China', 'Australia', 'Canada', 'US'].some(
    (el) => el === regionName
  )
  return passed
}

function normalizeName (locationName) {
  const found = normalizations.find(
    (element) => element[0].toUpperCase() === locationName.toUpperCase()
  )

  if (found && found[1]) {
    return found[1]
  }

  return locationName
}

async function getRemoteCSV (url, fileName) {
  try {
    let response

    if (
      process.env.NODE_ENV === 'development' &&
      fs.existsSync(fileName + '.csv')
    ) {
      response = fs.readFileSync(fileName + '.csv', 'utf-8')
      return response
    } else {
      try {
        response = await cachedAxios.get(url)
        fs.writeFileSync(fileName + '.csv', response.data)
      } catch (error) {
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
      }

      return response.data
    }
  } catch (error) {
    console.log(error)
  }
}
